package com.vividhelix.hwo.json;

public class Lane {
    private int distanceFromCenter;
    private int index;

    public int getDistanceFromCenter() {
        return distanceFromCenter;
    }

    public int getIndex() {
        return index;
    }
}

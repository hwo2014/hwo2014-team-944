package com.vividhelix.hwo.json;

public class TrackParams {
    public double maxDrift=60;
    public double decel = 0.98;
    public double accel = 0.02;
}

package com.vividhelix.hwo.json;

import java.util.List;

public class Track {
    private String name;
    private List<Piece> pieces;
    private List<Lane> lanes;

    public String getName() {
        return name;
    }

    public List<Piece> getPieces() {
        return pieces;
    }

    public List<Lane> getLanes() {
        return lanes;
    }

    public Lane getLaneByIndex(int laneIndex){
        for (Lane lane : lanes) {
            if (lane.getIndex()==laneIndex)
                return lane;
        }
        return null;
    }

}

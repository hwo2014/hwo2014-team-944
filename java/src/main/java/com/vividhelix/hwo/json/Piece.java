package com.vividhelix.hwo.json;

import com.google.gson.annotations.SerializedName;

public class Piece {
    private double length;
    @SerializedName("switch")
    private boolean switching;
    private int radius;
    private double angle;

    public double getLength(int laneDistanceFromCenter) {//TODO consider whether switching as well
        if (radius == 0)
            return length;
        return 2*Math.PI* (radius - Math.signum(angle)*laneDistanceFromCenter) * Math.abs(angle)/360d;
    }

    public boolean isSwitching() {
        return switching;
    }

    public int getRadius() {
        return radius;
    }

    public int getRadiusOnLane(Track track, PiecePosition piecePosition) {
        if (radius == 0)
            return 0;
        Lane lane = track.getLaneByIndex(piecePosition.getLane().getStartLaneIndex());
        int sign = angle > 0 ? 1 : -1;
        return radius - sign * lane.getDistanceFromCenter();
    }

    public double getAngle() {
        return angle;
    }

}

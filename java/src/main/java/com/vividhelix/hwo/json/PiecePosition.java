package com.vividhelix.hwo.json;

import java.util.List;

public class PiecePosition {
    private int pieceIndex;
    private double inPieceDistance;
    private int lap;
    private LanePosition lane;

    public int getPieceIndex() {
        return pieceIndex;
    }

    public double getInPieceDistance() {
        return inPieceDistance;
    }

    public double distanceFrom(PiecePosition previous, Track track){
        if (previous == null){
            return inPieceDistance;
        }
        if (pieceIndex == previous.getPieceIndex())
            return inPieceDistance - previous.inPieceDistance;
        return inPieceDistance + previous.remainingDistance(track);
    }

    public double remainingDistance(Track track){
        int laneDistanceFromCenter = track.getLaneByIndex(lane.getStartLaneIndex()).getDistanceFromCenter();
        return track.getPieces().get(pieceIndex).getLength(laneDistanceFromCenter) - inPieceDistance;
    }

    public int getLap() {
        return lap;
    }

    public LanePosition getLane() {
        return lane;
    }
}

package com.vividhelix.hwo.json;

public class LanePosition {
    private int startLaneIndex;
    private int endLaneIndex;

    public int getStartLaneIndex() {
        return startLaneIndex;
    }

    public int getEndLaneIndex() {
        return endLaneIndex;
    }
}

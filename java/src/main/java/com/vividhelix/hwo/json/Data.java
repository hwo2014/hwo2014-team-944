package com.vividhelix.hwo.json;

public class Data {
    private Race race;
    private PiecePosition piecePosition;
    private double angle;

    public Race getRace() {
        return race;
    }

    public PiecePosition getPiecePosition() {
        return piecePosition;
    }

    public double getAngle() {
        return angle;
    }
}

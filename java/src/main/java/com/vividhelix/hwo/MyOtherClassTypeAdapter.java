package com.vividhelix.hwo;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.vividhelix.hwo.json.Data;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

class MyOtherClassTypeAdapter implements JsonDeserializer<List<Data>> {
    public List<Data> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext ctx) {
        List<Data> vals = new ArrayList<Data>();
        if (json.isJsonArray()) {
            for (JsonElement e : json.getAsJsonArray()) {
                vals.add((Data) ctx.deserialize(e, Data.class));
            }
        } else if (json.isJsonObject()) {
            vals.add((Data) ctx.deserialize(json, Data.class));
        } else {
            throw new RuntimeException("Unexpected JSON type: " + json.getClass());
        }
        return vals;
    }
}

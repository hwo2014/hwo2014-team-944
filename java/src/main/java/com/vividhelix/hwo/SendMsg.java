package com.vividhelix.hwo;

import com.google.gson.Gson;

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new OutgoingMsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

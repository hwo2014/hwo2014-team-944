package com.vividhelix.hwo;

import com.vividhelix.hwo.json.Data;

import java.util.Collections;
import java.util.List;

class IncomingMsgWrapper {
    public final String msgType;
    public final List<Data> data;

    IncomingMsgWrapper(final String msgType, final Data data) {
        this.msgType = msgType;
        this.data = Collections.singletonList(data);
    }
}

package com.vividhelix.hwo;

public class JoinRace extends SendMsg {
    private BotId botId;
    private int carCount;
    private String trackName;

    public JoinRace(final String name, final String key, final int carCount, final String trackName) {
        this.carCount = carCount;
        this.trackName = trackName;
        this.botId = new BotId(name, key);
    }

    public static String finland(){
        return "keimola";
    }

    public static String germany(){
        return "germany";
    }

    public static String usa(){
        return "usa";
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }

    public BotId getBotId() {
        return botId;
    }

    public int getCarCount() {
        return carCount;
    }

    public String getTrackName() {
        return trackName;
    }

    private static class BotId {
        private String name;
        private String key;

        private BotId(String name, String key) {
            this.name = name;
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public String getKey() {
            return key;
        }
    }
}

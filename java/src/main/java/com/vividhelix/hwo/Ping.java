package com.vividhelix.hwo;

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

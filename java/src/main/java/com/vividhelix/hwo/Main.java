package com.vividhelix.hwo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.vividhelix.hwo.json.Data;
import com.vividhelix.hwo.json.Piece;
import com.vividhelix.hwo.json.PiecePosition;
import com.vividhelix.hwo.json.Track;

import java.io.*;
import java.lang.reflect.Type;
import java.net.Socket;
import java.util.List;

public class Main {

    private Track track;
    private double previousAngle;
    private double previousAngle2;
    private double previousAngle3;
    private double previousDistance;
    private double previousDistance2;
    private double previousRadius;
    private double previousRadius2;

    private double previousAngleDelta;
    private double distanceTravelled;
    private PiecePosition previousPiecePosition;

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new JoinRace(botName, botKey,1, JoinRace.usa()));
    }

    private PrintWriter writer;

    private int switched=2;
    private boolean switchLatch;
    private boolean headerPrinted;
    private boolean maxReached;
    int countInCurve;

    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg joinMsg) throws IOException {
        Type locationInfoListType = new TypeToken<List<Data>>() {
        }.getType();

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(locationInfoListType, new MyOtherClassTypeAdapter())
                .create();
        this.writer = writer;
        String line;

        send(joinMsg);
        PrintWriter pw = new PrintWriter(new FileWriter(System.currentTimeMillis() + ".csv"));

        while ((line = reader.readLine()) != null) {
            pw.flush();

            final IncomingMsgWrapper msgFromServer = gson.fromJson(line, IncomingMsgWrapper.class);
            if (!msgFromServer.msgType.equals("carPositions"))
                System.out.println("LINE: " + line);
            if (msgFromServer.msgType.equals("carPositions")) {
                if (switched>0 && !switchLatch) {
                    switchLatch=true;
                    switched --;
                    send(new Switch(false));
                    continue;
                }
                if (switchLatch && previousPiecePosition!=null && track.getPieces().get(previousPiecePosition.getPieceIndex()).isSwitching()){
                    switchLatch=false;
                }
                double angle = msgFromServer.data.get(0).getAngle();
                PiecePosition piecePosition = msgFromServer.data.get(0).getPiecePosition();
                double distance = piecePosition.distanceFrom(previousPiecePosition, track);
                previousPiecePosition = piecePosition;

                int pieceIndex = piecePosition.getPieceIndex();
                Piece piece = track.getPieces().get(pieceIndex);
//                A0 = 2.89103177832024*A1 + 0.899270331298372*A3 - 2.79030470956858*A2
                double a1=2.89103177832024;
                double a2=-2.79030470956858;
                double a3=0.899270331298372;
                double estAngle = a1*previousAngle + a2*previousAngle2 + a3*previousAngle3;
                double formula = angle-estAngle;
                distanceTravelled+=distance;
                double maxSpeed = 1;
                double thro2 = maxReached?maxSpeed:.903;
                if (!maxReached && distance / 10 > maxSpeed - 0.01){
                    maxReached = true;
                }

//                if (piece.getRadius()==0)
//                    countInCurve=0;
//                else
//                    countInCurve++;
//                if ((countInCurve/5)%2==1)
//                    thro2*=0.85;
                double pieceLength = piece.getLength(track.getLaneByIndex(piecePosition.getLane().getStartLaneIndex()).getDistanceFromCenter());

                System.out.println(String.format("THR: %10.5f, DISTTRVL:%10.5f SPD:%10.5f  ANG:%10.5f  ANGDELTA:%10.5f ANGDELTA2 %10.5f FORM:%15.5fRADIUS:%3d PLEN:%15.5f",
                        thro2,distanceTravelled, distance, angle, angle - previousAngle, angle - previousAngle - previousAngleDelta, formula, piece.getRadiusOnLane(track, piecePosition), pieceLength));
                if (!headerPrinted) {
                    headerPrinted = true;
                    pw.println("THRO, SPD2, SPD1, SPD,ANG3, ANG2, ANG1, ANG, RAD2, RAD1, RAD, FORM, PLEN");
                }

                pw.println(String.format("%30.25f,%30.25f,%30.25f,%30.25f,%30.25f,%30.25f,%30.25f,%30.25f,%30.25f,%30.25f,%30.25f,%30.25f, %30.25f",
                        thro2,previousDistance2, previousDistance, distance, previousAngle3, previousAngle2, previousAngle, angle, previousRadius2, previousRadius,
                        (double)piece.getRadiusOnLane(track, piecePosition), formula, pieceLength));
                previousAngleDelta = angle - previousAngle;
                previousAngle3=previousAngle2;
                previousAngle2=previousAngle;
                previousAngle = angle;

                previousDistance2=previousDistance;
                previousDistance=distance;

                previousRadius2=previousRadius;
                previousRadius=piece.getRadiusOnLane(track, piecePosition);

                send(new Throttle(thro2));
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                track = msgFromServer.data.get(0).getRace().getTrack();
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                send(new Ping());
            }
        }
        pw.close();
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}


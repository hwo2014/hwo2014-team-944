package com.vividhelix.hwo;

class Switch extends SendMsg{
    private String direction;

    Switch(boolean left) {
        this.direction = left?"Left":"Right";
    }

    @Override
    protected Object msgData() {
        return direction;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

package com.vividhelix.hwo;

class OutgoingMsgWrapper {
    public final String msgType;
    public final Object data;

    public OutgoingMsgWrapper(final SendMsg sendMsg) {
        this.msgType = sendMsg.msgType();
        this.data = sendMsg.msgData();
    }
}
